# Include this in a script using `. scripts/enter-virtualenv.sh`

echo "+++ Enabling virtualenv in ENV"
if [[ ! -d ENV ]]; then
    echo creating virtualenv
    virtualenv -p python3 ENV
fi
if [[ -z "$VIRTUAL_ENV" ]]; then
    echo activating virtualenv
    . ENV/bin/activate
fi

echo "+++ Checking/installing requirements"
pip install -q -r ./requirements.txt
