#!/usr/bin/env bash

# Exports a confluence space, cleans the html content and converts it into markdown. If it doesn't work for you,
# read the README.md in the project folder, you may be missing some dependencies.
#
# usage: export-space.sh <SPACENAME>
# where <SPACENAME> defaults to jiracloud

if [[ `uname` == 'Darwin' ]]; then
    export/sanity.py
    if [[ $? != 0 ]]; then
        exit 1
    fi
fi

space=${1:-jiracloud}

echo "exporting space $space"

basedir=`dirname $0`/../
pushd "$basedir" > /dev/null
rm -rf tmp/"$space"
mkdir -p tmp/"$space"
export/space-export.py -v https://developer.atlassian.com "$space" tmp |tee export.log
if [[ ${PIPESTATUS[0]} == 0 ]]; then
    export/clean-html.py -v tmp/"$space" "/$space" content/"$space" |tee -a export.log
fi
popd >/dev/null
