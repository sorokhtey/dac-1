The following repositories contain example Atlassian Connect add-ons. This list is growing all the time, so check back often.

Please be aware that this list only contains example add-ons. Go to the page on [Frameworks and Tools](../developing/frameworks-and-tools.html)
if you want to find frameworks, libraries and tools that you can use in your Atlassian Connect development.

<table class='aui aui-table-sortable'>
    <thead>
        <th>Add-on</th>
        <th>Language</th>
        <th>Framework(s)</th>
        <th class='aui-table-column-unsortable'>Description</th>
    </thead>
    <tbody>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-sequence-diagramr">Sequence Diagramr</a></td>
            <td>JavaScript</td>
            <td><a href="https://bitbucket.org/atlassian/atlassian-connect-express">Atlassian Connect Express (ACE)</a> - <a href="https://nodejs.org">Node.js</a></td>
            <td>A Confluence macro for creating UML sequence diagrams on the page.</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-word-cloud">Tim's Word Cloud</a></td>
            <td>JavaScript</td>
            <td><a href="https://bitbucket.org/atlassian/atlassian-connect-express">Atlassian Connect Express (ACE)</a> - <a href="https://nodejs.org">Node.js</a></td>
            <td>A macro that takes the contents of a page and constructs an SVG based word cloud.</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/webhook-inspector">Webhook Inspector</a></td>
            <td>JavaScript</td>
            <td><a href="https://bitbucket.org/atlassian/atlassian-connect-express">Atlassian Connect Express (ACE)</a> - <a href="https://nodejs.org">Node.js</a></td>
            <td>Inspects the response bodies of the available webhooks in Atlassian Connect.</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/robertmassaioli/ep-tool">Entity Property Tool</a></td>
            <td>JavaScript</td>
            <td><a href="https://nodejs.org">Node.js</a></td>
            <td>An Atlassian Connect add-on that allows manipulation of entity properties in JIRA.</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/atlassian/whoslooking-connect/overview">Who's Looking</a></td>
            <td>Java</td>
            <td><a href="https://bitbucket.org/atlassian/atlassian-connect-play-java">Atlassian Connect Play Java</a> - <a href="https://www.playframework.com/">Play Framework</a></td>
            <td>Displays the users who are currently looking at a JIRA issue.</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/atlassian-autowatch-plugin">JIRA Autowatcher</a></td>
            <td>Java</td>
            <td><a href="https://bitbucket.org/atlassian/atlassian-connect-play-java">Atlassian Connect Play Java</a> - <a href="https://www.playframework.com/">Play Framework</a></td>
            <td>Automatically add watchers to newly created JIRAs</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/robertmassaioli/hackathon">Hackathon</a></td>
            <td><a href="https://www.haskell.org/">Haskell</a></td>
            <td><a href="http://snapframework.com/">Snap framework</a></td>
            <td>An add-on to run any Hackathon via JIRA. A feature rich sample add-on with tight integration with JIRA.
                This is the add-on that Atlassian uses for <a href="https://www.atlassian.com/company/shipit">ShipIt</a>.
            </td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/my-reminders/overview">My Reminders</a></td>
            <td><a href="https://www.haskell.org/">Haskell</a></td>
            <td><a href="http://snapframework.com/">Snap framework</a></td>
            <td>Sends you reminders about JIRA issues</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/acgmaps/acgmaps.bitbucket.org">Google Maps macro</a></td>
            <td>HTML / CSS / JavaScript (Static add-on)</td>
            <td>No framework<br />(Static add-on)</td>
            <td>Insert Google Maps into your Confluence pages</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/robertmassaioli/sketchfab-connect">Sketchfab for Confluence</a></td>
            <td>HTML / CSS / JavaScript (Static add-on)</td>
            <td>No framework<br />(Static add-on)</td>
            <td>A static Atlassian Connect add-on that renders Sketchfab 3D models in Confluence pages</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/atlassian-connect-whoslooking-connect-v2">Who's Looking Front-End Only Prototype</a></td>
            <td>HTML / CSS / JavaScript (Static add-on)</td>
            <td>No framework<br />(Static add-on)</td>
            <td>Similar functionality to the original [Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect/overview) Play-based
                add-on, but written entirely using static HTML, JS & CSS using [Firebase](https://www.firebase.com/).</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/whoslooking-connect-scala">Who's Looking (Play Scala)</a></td>
            <td><a href="http://www.scala-lang.org/">Scala</a></td>
            <td><a href="https://www.playframework.com/">Play Scala framework</a></td>
            <td>Same functionality to the original [Who's Looking](https://bitbucket.org/atlassian/whoslooking-connect/overview) Play-based
                Java add-on, but written in Scala, built on the Play Scala framework.</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/atlas-lingo">Atlas Lingo</a></td>
            <td><a href="http://www.scala-lang.org/">Scala</a></td>
            <td><a href="https://www.playframework.com/">Play Scala framework</a></td>
            <td>Translate JIRA issues into other languages using Google Translate API.</td>
        </tr>
    </tbody>
</table>

*Note:* A simple Atlassian Connect addon can consist of entirely static content (HTML, CSS and JavaScript). 
These add-ons do not have a framework in the traditional sense and have been marked as "Static add-on" in this list.

 [8]: http://www.scala-lang.org/