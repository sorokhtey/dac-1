# Frameworks and tools

Atlassian Connect add-ons can be written using many different languages, frameworks and tools. Since Atlassian Connect 
add-ons operate remotely over HTTP and can be written with any programming language and web framework there are many
tools available for you to develop your add-ons.

<div class="aui-message note">
    <div class="icon"></div>
    <p class="title">
        <strong>Note</strong>
    </p>
    <p>
    View the listings on the Sample Add-ons page to see some examples of these libraries and frameworks being used.
    </p>
</div>

<a name='frameworks'></a>
## Frameworks

We've written two different frameworks to help you get started. These frameworks help to generate some of the plumbing
required for your Connect add-on, and are officially supported by Atlassian:

 * [Node.js](https://bitbucket.org/atlassian/atlassian-connect-express)
 * [Spring Boot](https://bitbucket.org/atlassian/atlassian-connect-spring-boot/overview)
  
Atlassian and our excellent developer community have also written a number of other frameworks that you can use. None of 
these frameworks are supported by Atlassian but they may be supported by members of the community:

 * [Play (Java)](https://bitbucket.org/atlassian/atlassian-connect-play-java)
 * [Play (Scala)](https://bitbucket.org/atlassianlabs/atlassian-connect-play-scala)
 * [.NET](https://bitbucket.org/atlassianlabs/atlassian-connect-.net)
 * [Symfony2 Atlassian Connect Bundle (PHP)](https://github.com/thecatontheflat/atlassian-connect-bundle)
 * Dart
   * _Atlassian Connect JWT library_ - [atlassian_connect_jwt](https://pub.dartlang.org/packages/atlassian_connect_jwt)
   * _Services for handling communications with the host product_ - [atlassian_connect_host](https://pub.dartlang.org/packages/atlassian_connect_host)
   * _Helpers for managing environment configuration_ - [atlassian_connect_config](https://pub.dartlang.org/packages/atlassian_connect_config)
   * _Simple web server bundling the above components_ - [atlassian_connect_shelf](https://pub.dartlang.org/packages/atlassian_connect_shelf)
 * Haskell
   * [atlassian-connect-core](http://hackage.haskell.org/package/atlassian-connect-core)
   * [atlassian-connect-descriptor](http://hackage.haskell.org/package/atlassian-connect-descriptor)

<a name='tools'></a>
## Tools

The following tools can be of great help when implementing add-ons.

<table class='aui'>
	<thead>
		<th>Tool</th>
		<th>Description</th>
	</thead>
	<tr>
		<td><a href="https://atlassian-connect-validator.herokuapp.com/validate">JSON descriptor validator</a></td>
		<td>This validator will check that your descriptor is syntactically correct. Just paste the JSON content 
			of your descriptor in the "descriptor" field, and select the Atlassian product you want to validate 
			against.</td>
	</tr>
	<tr>
		<td><a href="http://jwt-decoder.herokuapp.com/jwt/decode">JWT decoder</a></td>
		<td>An encoded JWT token can be quite opaque. You can use this handy tool to decode JWT tokens and inspect 
			their content. Just paste the full URL of the resource you are trying to access, including the JWT
			token, in the URL field, e.g. https://example.atlassian.net/path/to/rest/endpoint?jwt=token</td>
	</tr>
	<tr>
	    <td><a href="https://marketplace.atlassian.com/plugins/com.atlassian.connect.entity-property-tool.prod">Entity property tool</a> and <a href="https://marketplace.atlassian.com/plugins/com.atlassian.connect.confluence-property-tool">Content property tool</a></td>
	    <td><a href="../concepts/hosted-data-storage.html">Hosted data storage</a> is one of the most useful features of Atlassian 
	    Connect add-on development. It lets you store data against the host product without requiring a backend. In JIRA
	    hosted data storage is implemented via Entity Properties and the Entity Property tool makes it trivial to create,
	    read, update and delete Entity Properties in JIRA. In Confluence hosted data storage is implemented via Content 
	    Properties and the Content property tool makes it trivial to create, read, updated and delete Content Properties in
	    Confluence.
	    </td>
	</tr>
	<tr>
        <td>Web fragment finder</td>
        <td>Available for <a href="https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder">JIRA</a> or for <a href="https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder-confluence/cloud/overview">Confluence</a>.  
        
        The web fragment finder is an add-on which loads a <i>Web Fragment</i> (which can be a
        <i>Web Item</i>, <i>Web Section</i> or <i>Web Panel</i>) in all available locations that JIRA or Conlfuence Connect provides.
        All Web Fragments contain their location, making it easier to search, and use the right extension points for your add-on.   
          
        All Web Fragments have a unique <i>location</i> and it
        can be tricky to know which location to use. The web fragment finder add-on highlights
        many of the available locations, making it easy to find the location that you are looking for.
        </td>
    </tr>
    <tr>
        <td><a href="https://connect-inspector.atlassian.io/">Connect inspector</a></td>
        <td>The Connect inspector allows you to generate a temporary Atlassian Connect add-on that you can install into
        your Cloud Development Environment. It is temporary because it will only live for three days; after which all of 
        the data it stores will be wiped. It will store any lifecycle and webhook events that it receives; and it registers
        all webhook events against the system. This means that it supersedes the Webhook Inspector for Atlassian Connect
        development. It is an extremely useful tool for any developer wanting to get insight into the lifecycle and 
        webhook events as you can watch the events happen live in your web browser.</td>
    </tr>
    <tr>
        <td><a href="https://github.com/minhhai2209/jira-plugin-converter">JIRA Plugin Converter</a></td>
        <td>This is a tool to convert an Atlassian Connect add-on's descriptor to a JIRA Server plugin. The remote server 
        is shared between both. In this way, developers can build add-ons for JIRA Cloud first, then generate plugins 
        for JIRA Server later.</td>
    </tr>
</table>
