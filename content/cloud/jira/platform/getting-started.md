---
title: Getting started
platform: cloud
product: jiracloud
category: devguide
subcategory: intro
aliases:
- /jiracloud/getting-started-39988011.html
- /jiracloud/getting-started-39988011.md
confluence_id: 39988011
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988011
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988011
date: "2016-09-24"
---
# Getting started

Welcome to JIRA Cloud development! This tutorial is designed to help you learn the basics of development for the JIRA Cloud products (i.e. JIRA Core, JIRA Software, and JIRA Service Desk), using Atlassian Connect. Atlassian Connect is our recommended framework for extending JIRA Cloud, and handles discovery, installation, authentication, and seamless integration into the JIRA UI.

By the end of this tutorial, you'll have set up everything you need to start developing for JIRA Cloud. This includes setting up your local development environment for Atlassian Connect, getting a JIRA Cloud development instance, and validating your setup by building and deploying a basic "Hello World" add-on.

**On this page:**

-   [Before you begin](#before-you-begin)
-   [Set up a development environment](#set-up-a-development-environment)
-   [Build a basic add-on](#build-a-basic-add-on)
-   [(optional) Build a JIRA add-on using a framework]({{< relref "#jira-addon" >}})
-   [Next steps](#next-steps)

## Before you begin

To complete this tutorial, you'll need the following:

-   Your favorite IDE or text editor.
-   A basic knowledge of JavaScript and web development.
-   A basic understanding of JIRA.
-   A running Node.js environment (v4.5.0 or later required). See the [Node.js downloads page], if you need to install it. Note, Node.js bundles npm, which you'll also need.

## Set up a development environment

To develop for JIRA Cloud, you'll need two things: a local development environment to build add-ons and an Atlassian Cloud instance to test them in.

### Step 1. Get an Atlassian Cloud instance 

Let's start by getting you an Atlassian Cloud instance that you can install and test your add-ons on. 

1.  Go to [go.atlassian.com/cloud-dev] and sign up for a free development environment. It will take a few minutes to provision.
2.  Once your Atlassian Cloud instance is ready, sign in and complete the setup wizard. 
3.  Finally, enable development mode for your Atlassian Cloud instance. This lets you install add-ons in your instance that are not from the Atlassian Marketplace:
    1.  Navigate to JIRA administration (**cog icon **in the header) &gt; **Add-ons **&gt; **Manage add-ons**.
    2.  Scroll to the bottom of the 'Manage add-ons' page and click **Settings**.
    3.  Select the **Enable development mode** checkbox.

Your Atlassian Cloud development instance has Confluence and all of the JIRA products installed, but be aware that Atlassian Cloud development instances have limits on the number of users and are not supported by Atlassian.

### Step 2. Set up your local development environment

If you install an Atlassian Connect add-on in an Atlassian Cloud instance, the add-on itself is usually hosted elsewhere (e.g. on a service like Heroku). However, when you are building an add-on, it's easiest to develop it on your local machine and make it available over the internet (via HTTPS) via tunnelling. This allows you to test it against an Atlassian Cloud instance, but still work locally. 

There are a number of tools that you can use to tunnel your local development environment to the internet, but we recommend **ngrok**, which is free and easy to use.

1.  Download and install ngrok:
    -   If you have [npm] installed, use: `npm install -g ngrok`
    -   Otherwise, download it from the [ngrok] website and unzip it.
2.  Check that it's working by running `ngrok help` in your terminal. 

That's all the setup that you need to do for now. We'll show you how to use ngrok to make your add-on available to the internet later in this tutorial, when you deploy your add-on.

## Build a basic add-on

In this section, you'll build a basic add-on by turning a simple web application into an Atlassian Connect add-on. This add-on will be for JIRA Cloud, but the process is similar for other Atlassian products. This will give you a hands-on introduction to Atlassian Connect and will validate that your development environment is set up correctly.

### Step 1. Define an add-on descriptor

The fundamental building block of an add-on is the JSON descriptor file, usually named `atlassian-connect.json`. This file describes your add-on to the Atlassian application, including your add-on's key, name, permissions needed to operate, and the different modules it uses for integration.

In the steps below, you'll define the `atlassian-connect.json` file for a JIRA Cloud add-on. It will use a [`generalPages` module], and add a link to JIRA's top navigation element titled "Greeting". 

1.  Create a project directory for your add-on source files. You'll be working in this directory for the rest of this tutorial.
2.  In your project directory, create a new file named `atlassian-connect.json` with the following contents:

    ``` json
    {
         "name": "Hello World",
         "description": "Atlassian Connect add-on",
         "key": "com.example.myaddon",
         "baseUrl": "https://<placeholder-url>",
         "vendor": {
             "name": "Example, Inc.",
             "url": "http://example.com"
         },
         "authentication": {
             "type": "none"
         },
         "apiVersion": 1,
         "modules": {
             "generalPages": [
                 {
                     "url": "/helloworld.html",
                     "key": "hello-world",
                     "location": "system.top.navigation.bar",
                     "name": {
                         "value": "Greeting"
                     }
                 }
             ]
         }
     }
    ``` 
    Note, you don't need to change the placeholder used for `baseUrl` for now. You'll update it later in this tutorial when you're ready to deploy your add-on.
4.  Save and close the descriptor file.
5.  (optional) Validate your descriptor using the [Atlassian Connect validator]. This handy tool shows you any errors in your add-on descriptor, such as missing properties, syntax errors, etc.

### Step 2. Create a web application

Now that you've created the add-on descriptor, let's create a web app. We'll be using a simple static HTML page as the "app". This is the most basic Atlassian Connect integration: an add-on descriptor and an HTML page that it uses. It's not a typical add-on, but once you understand how it works, it takes only a few more steps to turn a web application into a fully fledged Atlassian Connect add-on.

1.  In your project directory, create a new file named `helloworld.html`. You'll notice that this is the same filename referenced in the `url` element of your descriptor file.
2.  Add the following content to the `helloworld.html` file:

    ``` html
    <!DOCTYPE html>
    <html lang="en">
     <head>
         <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.9.12/css/aui.min.css" media="all">
     </head>
     <body>
         <section id="content" class="ac-content">
             <div class="aui-page-header">
                 <div class="aui-page-header-main">
                     <h1>Hello World</h1>
                 </div>
             </div>
         </section>

         <script id="connect-loader" data-options="sizeToParent:true;">
             (function() {
                 var getUrlParam = function (param) {
                     var codedParam = (new RegExp(param + '=([^&]*)')).exec(window.location.search)[1];
                     return decodeURIComponent(codedParam);
                 };

                 var baseUrl = getUrlParam('xdm_e') + getUrlParam('cp');
                 var options = document.getElementById('connect-loader').getAttribute('data-options');

                 var script = document.createElement("script");
                 script.src = baseUrl + '/atlassian-connect/all.js';

                 if(options) {
                     script.setAttribute('data-options', options);
                 }

                 document.getElementsByTagName("head")[0].appendChild(script);
             })();
         </script>

     </body>
    </html>
    ```
3.  Save your file.

That's all the coding you need to do. Let's have a look at the content of the `helloworld.html` file in more detail:

-   **Styling**— The HTML page uses [Atlassian User Interface (AUI)], by including the relevant CSS from the AUI CDN. AUI is a library of reusable front-end UI components.
-   **The `ac-content` class**— This class wraps the content of your add-on and dynamically resizes the iframe in JIRA. This keeps your add-on content visible without pesky scrollbars.
-   **The `script` tag** — The `all.js` file is included in the HTML page via a `script` tag. This file is the client library for the [Atlassian Connect JavaScript API], and is hosted on every Atlassian Cloud application. The Atlassian Connect JavaScript API simplifies client interactions with the Atlassian application, like making an XMLHttpRequest.
    The `all.js` file can be found at the following URL for your Atlassian application: `//HOSTNAME.atlassian.net/CONTEXT/atlassian-connect/all.js` This URL is constructed from the following parameters:
    -   HOSTNAME: The hostname for the Atlassian application.
    -   CONTEXT: The application context for the application. `jira` is the default context (when none is provided) and wiki is used for Confluence.
    Note, within your iframe, you must get the `all.js` script from the product domain. The code within the `script` tag above, is an easy way to do this without a having to re-create the HTML through a template on each call. For more information, check out the [Atlassian Connect JavaScript API].

### Step 3. Deploy your add-on

You now have your first Atlassian Connect add-on! Let's get it running in your Atlassian Cloud instance. To deploy it, you'll be hosting it on a web server, using ngrok to make it available to the internet, then installing your add-on in your Atlassian Cloud instance.

#### Step 3a. Host your add-on on a web server

You'll need a simple web server to serve the current directory containing your `atlassian-connect.json` and `helloworld.html` files. There are number of tools you can use to do this, but in this tutorial, we'll be using [http-server] (available via [npm]).

1.  Install **http-server** via the following npm command, by running the following command from your terminal:

    ``` shell
    npm install http-server -g
    ```
2.  In your project directory, start your server on port 8000, by running the following command from your terminal:

    ``` shell
    http-server -p 8000
    ```
    You'll see a message in your terminal indicating that the server is serving HTTP at the current address and port. It will look something like this:
    "`Starting up http-server, serving ./ on: http://0.0.0.0:8000/`".
3.  Confirm that the files you created in steps 1 and 2 are being served. Visit the following URLs and you should be able to view your add-on files:
    -   `http://localhost:8000/atlassian-connect.json`
    -   `http://localhost:8000/helloworld.html`

#### Step 3b. Make your add-on files available to the internet

Now that your add-on is hosted on a local web server, you need to make it available over the internet. The following steps describe how to do this using ngrok. Note, you don't need to do step 1, as we already know that your add-on is running on port 8000, but it's useful to know for future add-on development.

1.  Find out what port your add-on is running on, by doing either of the following:
    -   Run the add-on and look for the port in the logs
    -   Check the project README for details
2.  Run the following command from your terminal. This will expose your web server to the internet. If your add-on is not running on port 8000, change the command to use your add-on's port number.

    ``` shell
    ngrok http 8000
    ```
    You'll also be shown a status page in your terminal (see next step) that shows the public URL of your tunnel and other information about connections made over your tunnel.

<div class="aui-message note">
    <div class="icon"></div>
    <p class="title">
        <strong>Note</strong>
    </p>
   <p>
   If your add-on is not running when you try to start ngrok, you'll see a "Failed to complete tunnel connection" message.
   </p>
</div>

3.  Get the HTTPS URL from the ngrok status page, as shown in the following image:
    ![Alt text](../images/ngrok-example-shadow-arrow.jpg)
4.  Edit your add-on's descriptor and set the `baseUrl` property to the ngrok HTTPS URL (from the previous step). e.g.

    ``` json
    "baseUrl": "https://02b76172.ngrok.io"
    ```

<div class="aui-message note">
    <div class="icon"></div>
    <p class="title">
        <strong>Note</strong>
    </p>
    <p>
   If you build more add-ons in future, note that the <code>baseUrl</code> attribute must be unique to each add-on.
   </p>
</div>   

5.  Confirm the descriptor is available from the ngrok HTTPS URL. This is the URL you will use to install your add-on in your Atlassian Cloud instance in the next section.
    If you were using the HTTPS URL in the image above, your descriptor URL would look something like this: `https://02b76172.ngrok.io/atlassian-connect.json`. 

#### Step 3c. Install your add-on

We're nearly there! The final step in deploying your add-on is to install it in your Atlassian Cloud instance. You'll do this by adding a link to your add-on's descriptor file from your Atlassian Cloud instance. This allows JIRA to install your add-on.

1.  Navigate to JIRA in your Atlassian Cloud instance, then choose the **cog menu** &gt; **Add-ons** .
2.  Click **Manage add-ons** **&gt;**  **Upload add-on**.
3.  Enter the link to your add-on descriptor. This URL should match the hosted location of your `atlassian-connect.json` descriptor file. e.g.

    ``` shell
    https://02b76172.ngrok.io/atlassian-connect.json
    ```
4.  Click **Upload**. JIRA will display the 'Installed and ready to go' dialog when the installation is complete.
5.  Click **Close**.
6.  Verify that your add-on appears in the list of *User installed add-ons*. For example, if you used Hello World for your add-on name, 'Hello World' should appear in the list.
7.  Reload the page. A new item labelled 'Greeting' will appear in the header.
8.  Click **Greeting**. A "Hello World" message will be displayed on a page.

<div class="aui-message tip">
    <div class="icon"></div>
    <p class="title">
        <strong>Congratulations!</strong>
    </p>
    <p>
    You've just built and deployed your first Atlassian Connect add-on!
    </p>
</div>

You now know enough to start developing add-ons with Atlassian Connect. Feel free to stop this tutorial here and head to the [next steps](#next-steps). However, if you'd like to learn more about how to streamline your development using Atlassian Connect frameworks, read on.

---

## (optional) Build a JIRA add-on using a framework {#jira-addon}

Frameworks help to generate some of the plumbing required for your Connect add-on, making it easier to create an add-on and simplifying the development process. There are a range of Atlassian Connect frameworks available for different languages, frameworks and tools, but in this section, we'll use one of the supported ones: [Atlassian Connect Express].

Atlassian Connect Express (ACE) is the official Atlassian Connect framework for [Node.js]. In this section, you'll set up ACE, then build a basic JIRA Cloud add-on using it. This will be a fully fledged Atlassian Connect add-on, not just a web app. As a bonus, you'll be able to use it as a starting point for any of the other JIRA Cloud tutorials in this documentation.

### Step 1. Install Atlassian Connect Express (ACE)

You'll need **npm** to install ACE.

1.  Install the atlas-connect CLI tool by running the following command:

    ``` shell
    npm i -g atlas-connect
    ```
2.  Test that atlas-connect has installed correct by checking the version:

    ``` shell
    atlas-connect -V
    ```
     Your terminal should show something like this: `0.6.4.`

### Step 2. Create a JIRA add-on using ACE

We're ready to build an add-on using ACE! This JIRA Cloud add-on will be about as basic as it gets, but you'll be able to learn the fundamental steps of the process.

1.  Create an add-on project using the `atlas-connect` command. We're using the project name "jira-getting-started" in this tutorial. Run this in any directory, except for the project directory you used for your previous add-on above.

    ``` shell
    atlas-connect new -t jira jira-getting-started
    ```
    This command will generate the basic skeleton for your atlassian-connect-express enabled add-on, in a new **jira-getting-started** directory:

    ``` shell
        .
        ├── README.md
        ├── app.js
        ├── atlassian-connect.json
        ├── config.json
        ├── credentials.json.sample
        ├── package.json
        ├── private-key.pem
        ├── public-key.pem
        ├── package.json
        ├── public
        │   ├── css
        │   │   └── addon.css
        │   └── js
        │       └── addon.js
        ├── routes
        │   └── index.js
        └── views
            ├── hello-world.hbs

            ├── layout.hbs

            └── unauthorized.hbs
    ```        
2.  Change to the **jira-getting-started** directory and install all required dependencies:

    ``` shell
    npm install
    ```

That's it! You now have an Atlassian Connect add-on. The `atlas-connect new` command actually creates a simple "Hello World" dialog in your new add-on, which you'll see when we deploy it to JIRA in the next step.

### Step 3. Deploy your JIRA add-on

You have a JIRA Cloud instance and you have an add-on. It's time to put the two together. 

1.  In your **jira-getting-started** directory, copy the `credentials.json.sample` file to a new `credentials.json` file. Edit the `credentials.json` file and update the URL, username, and password to match your JIRA Cloud instance, then save it. It should look something like this:

    ``` shell
    {
        "hosts": {
            "<your development instance URL goes here>": {
                "product": "jira",
                "username": "admin",
                "password": "examplepassword"
            }
        }
    }
    ```

<div class="aui-message note">
    <div class="icon"></div>
    <p class="title">
        <strong>Note</strong>
    </p>
    <p>
    Note, the <code>username</code> is not the same as the email address you signed up with. The default username is "admin".
    </p>
</div>
  
2.  We're ready to deploy your add-on! Run the following command:

    ``` shell
    npm start
    ```
    This will boot up your Express server on the default port of 3000. Here's where you'll see more benefits of using a framework, as ACE will also do the following for you:
    -   Create an ngrok tunnel to your local web server (ngrok is bundled with ACE and doesn't need to be configured separately).
    -   Register your add-on's `atlassian-connect.json` (at `http://<temp-ngrok-url>.io/atlassian-connect.json`) with your host JIRA Cloud instance.
    -   Start watching for changes to your `atlassian-connect.json`. If the file is modified, `atlassian-connect-express` will re-register your add-on with the host.
3.  Finally, check that that your add-on is working correctly. Navigate to your JIRA Cloud instance and you'll see a **Hello World** link in the header. Click it and you should see a page like this:
    ![Alt text](../images/jiradev-helloworld.png)

<div class="aui-message tip">
    <div class="icon"></div>
    <p class="title">
        <strong>Congratulations!</strong>
    </p>
    <p>
    You've just built an Atlassian Connect add-on using the Atlassian Connect Express framework!
    </p>
</div>

---

## Next steps

If you'd like to keep learning about add-on development for JIRA Cloud, the next step is to extend a JIRA application with an add-on. Try one of the tutorials linked below:

-   JIRA Software: [Adding a board configuration page]
-   JIRA Service Desk: [Extending the agent view via the JIRA platform]

Each of these tutorials will allow you to use the basic add-on that you've already built and extend it to add your own UI elements.

If you need help, head to the [Atlassian Developer Help Centre﻿]. You may also want to check out the Connect topics on [Atlassian Answers] and join our [Atlassian Connect mailing list].

  [Node.js downloads page]: https://nodejs.org/en/download/
  [ngrok]: https://ngrok.com/
  [npm]: https://www.npmjs.com/
  [go.atlassian.com/cloud-dev]: http://go.atlassian.com/cloud-dev
  [Atlassian Connect validator]: https://atlassian-connect-validator.herokuapp.com/validate
  [Atlassian User Interface (AUI)]: https://docs.atlassian.com/aui/  
  [`generalPages` module]: /cloud/jira/platform/page
  [Atlassian Connect JavaScript API]: /cloud/jira/platform/javascript-api
  [Atlassian Connect Express]: https://bitbucket.org/atlassian/atlassian-connect-express
  [http-server]: https://www.npmjs.com/package/http-server
  [Node.js]: https://nodejs.org  
  [Adding a board configuration page]: /cloud/jira/software/adding-a-board-configuration-page
  [Extending the agent view via the JIRA platform]: /cloud/jira/service-desk/extending-the-agent-view-via-the-jira-platform
  [Integrating with JIRA Cloud]: /cloud/jira/platform/integrating-with-jira-cloud
  [JIRA Software Cloud development]: /cloud/jira/software
  [JIRA Service Desk Cloud development]: /cloud/jira/service-desk
  [JIRA platform tutorials]: /cloud/jira/platform/jira-platform-tutorials
  [JIRA Software tutorials]: /cloud/jira/software/jira-software-tutorials
  [JIRA Service Desk tutorials]: /cloud/jira/service-desk/jira-service-desk-tutorials
  [Atlassian Developer Help Centre﻿]: https://ecosystem.atlassian.net/servicedesk/customer/portals
  [Atlassian Answers]: https://answers.atlassian.com/tags/atlassian-connect
  [Atlassian Connect mailing list]: https://groups.google.com/forum/?fromgroups=#!forum/atlassian-connect-dev 
