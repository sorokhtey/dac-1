#!/usr/bin/env python

import sys
import platform
import imp
from distutils.spawn import find_executable

"""
Sanity checking environment stuff for great good.
"""


def ansi(code):
    return lambda text: '\033[%dm%s\033[0m' % (code,  text)


green = ansi(92)
blue = ansi(94)
yellow = ansi(93)
magenta = ansi(95)
bold = ansi(1)
under = ansi(4)
red = ansi(91)

CHECK = green('\u2714')
CROSS = red('\u2716')


def missing(name, instructions):
    print("You do not have " + magenta(name) + " installed! " + CROSS)
    if instructions:
        print(" You should " + bold(instructions))
    exit(1)


def checkexe(name, instructions=False):
    instructions = instructions or "brew install " + name
    if find_executable(name):
        print("checking " + magenta(name) + " " + CHECK)
    else:
        missing(name, instructions)


def brew_installed():
    if platform.system() == "Darwin":
        checkexe("brew", "follow via instructions at http://brew.sh/")


def min_python(min_major):
    """
    Insist on minimum python version
    :param min_major: major version number
    """
    i = sys.version_info
    is_mac = platform.system() == "Darwin"
    got_brew = is_mac and find_executable("brew")

    if i.major < min_major:
        print(CROSS + " This is " + magenta("Python %d" % i.major) + " but you need %d.x" % min_major)

        if i.major == 3 and not find_executable("python3") and got_brew:
            print(" you're on " + bold("\uf8ff Mac") + " and have " + magenta("brew") + " so "
                  + green("brew install python3"))
        sys.exit(1)
    else:
        version_string = "%d.%d.%d" % (i.major, i.minor, i.micro)
        print(magenta("python") + " version: " + version_string + " " + CHECK)


def modules(modz):
    """
    Insist that the given list of modules are installed.
    :param modz: list of module names
    """
    for module in modz:
        try:
            imp.find_module(module)
            print("module " + magenta(module) + " " + CHECK)
        except ImportError:
            print("missing " + magenta(module) + " module " + CROSS)


def main():
    min_python(3)
    # mac specific for now
    brew_installed()
    checkexe("pandoc")
    checkexe("git-lfs")
    checkexe("node")
    modz = [
        "bs4",
        "requests",
        "pypandoc",
        "pytest",
        "yaml",     # installed as pyyaml
        "pyquery"
    ]
    modules(modz)

if __name__ == '__main__':
    sys.exit(main())

