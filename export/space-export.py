#!/usr/bin/env python

import argparse
import logging as log
import getpass
import sys
import dac


def get_options():

    parser = argparse.ArgumentParser(description='Export to markdown a Confluence space')
    parser.add_argument('root',
                        metavar='root',
                        help='confluence instance root url')
    parser.add_argument('space',
                        metavar='space',
                        help='the space slug')
    parser.add_argument('destination',
                        metavar='destination',
                        help='the directory into which the space export will be written')

    parser.add_argument('-u', '--username',
                        help='confluence username, otherwise guessed from local username')
    parser.add_argument('-p', '--password',
                        help='password for confluence instance, otherwise prompted')

    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        help='verbose: print out debug information')
    options = parser.parse_args()

    log.basicConfig(level=log.DEBUG if options.verbose else log.INFO, format=dac.LOG_FORMAT)

    return options


def main():
    opt = get_options()
    username = opt.username or getpass.getuser()
    password = opt.password or getpass.getpass()

    conf = dac.Confluence(opt.root, username, password)
    conf.fetch_space(opt.space, opt.destination, dac.unzip)


if __name__ == '__main__':
    sys.exit(main())
